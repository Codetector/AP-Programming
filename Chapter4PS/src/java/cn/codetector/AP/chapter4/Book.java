package cn.codetector.AP.chapter4;


public class Book {
    private int numberPages;
    private int currentPage;

    public Book(int numberPages) {
        this.numberPages = numberPages;
        this.currentPage = 1;
    }

    public int getNumberPages() {
        return numberPages;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void setNumberPages(int numberPages) {
        this.numberPages = numberPages;
    }

    public void nextPage(){
        if (currentPage<numberPages)currentPage++;
    }
}
