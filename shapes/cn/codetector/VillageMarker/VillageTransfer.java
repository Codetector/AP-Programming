package cn.codetector.VillageMarker;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class VillageTransfer implements Serializable{
	public int radius;
	public int D;
	public myVec3 vCenter;
	public List<myVec3> doors = new ArrayList();
	public VillageTransfer(int x, int y, int z, int r, int d){
		this.vCenter = new myVec3(x,y,z);
		this.radius = r;
		this.D = d;
	}
	
	public void addDoor(myVec3 door){
		this.doors.add(door);
	}

	public myVec3 getvCenter() {
		return vCenter;
	}

	public void setvCenter(myVec3 vCenter) {
		this.vCenter = vCenter;
	}

	public List<myVec3> getDoors() {
		return doors;
	}

	public void setDoors(List<myVec3> doors) {
		this.doors = doors;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
	
}
