package cn.codetector.VillageMarker;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.BlockPos;
import net.minecraft.village.*;

import org.lwjgl.opengl.GL11;

public class VillageMarker {
	public static Minecraft mc = Minecraft.getMinecraft();
	public static List<VillageTransfer> Villages_OWorld = new ArrayList();
	public static List<VillageTransfer> Villages_NWorld = new ArrayList();
	public static List<VillageTransfer> Villages_EWorld = new ArrayList();

	
	public static void renderVillageMarker(Tessellator tess,WorldRenderer wr, double xOff, double yOff, double zOff)
	{
		if (mc.isIntegratedServerRunning())
		{
			renderSinglePlayer(tess, wr, xOff, yOff, zOff);
		}else{
			if(mc.thePlayer.worldObj.provider.getDimensionId() == 0){
				renderSpheresAndLines(tess, wr, xOff, yOff, zOff, Villages_OWorld);
			}else if(mc.thePlayer.worldObj.provider.getDimensionId() == 1){
				renderSpheresAndLines(tess, wr, xOff, yOff, zOff, Villages_EWorld);
			}else{
				renderSpheresAndLines(tess, wr, xOff, yOff, zOff, Villages_NWorld);
			}
		}
	}
	
	public static void Process(byte[] b){
		System.out.println("--------------------");
		ByteArrayInputStream bis = new ByteArrayInputStream(b);
		try {
			ObjectInputStream ois = new ObjectInputStream(bis);
			Object o = ois.readObject();
			
			if(o instanceof VillageList){
				VillageList<VillageTransfer> vts = (VillageList<VillageTransfer>) o;
				System.out.println(vts.size());
				if(vts.getD() == 0){
					Villages_OWorld = vts;
				}else if(vts.getD() == -1){
					Villages_NWorld = vts;
				}else{
					Villages_EWorld = vts;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static List convert(List villages){
		List doneList = new ArrayList();
		
		List<Village> vlgs = villages;
		for(Village v : vlgs){
			VillageTransfer vt = new VillageTransfer(v.func_180608_a().getX(), v.func_180608_a().getY(), v.func_180608_a().getZ(), v.getVillageRadius(), -2);
			for(VillageDoorInfo d : (List<VillageDoorInfo>)v.getVillageDoorInfoList()){
				vt.addDoor(new myVec3(d.func_179852_d().getX(),d.func_179852_d().getY(),d.func_179852_d().getZ()));
			}
			doneList.add(vt);
		}
		
		return doneList;
	}
	
	private static void renderSinglePlayer(Tessellator tess,WorldRenderer wr, double xOff, double yOff, double zOff){
		List villageList = new ArrayList();
		villageList.addAll(mc.getIntegratedServer().getEntityWorld().getVillageCollection().getVillageList());
		renderSpheresAndLines(tess, wr, xOff, yOff, zOff, convert(villageList));
	}
	

	private static void renderSpheresAndLines(Tessellator tess,WorldRenderer wr, double xOff, double yOff, double zOff,List villageList)
	{
		
		Iterator villageListIterator = villageList.iterator();

		int c = 0;
		while (villageListIterator.hasNext())
		{
			Object vObj = villageListIterator.next();
			if ((vObj instanceof VillageTransfer))
			{
				List<myVec3> Doors = new ArrayList();
				int vRad = ((VillageTransfer)vObj).getRadius();
				myVec3 vCen = ((VillageTransfer)vObj).getvCenter();

				Doors.addAll(((VillageTransfer)vObj).getDoors());

				GL11.glEnable(3042);
				GL11.glDisable(3553);

				GL11.glBlendFunc(1, 32769);
				setColor(c % 6);

				c++;
				GL11.glPointSize(3);
				GL11.glLineWidth(3.0F);

				GL11.glEnable(2832);

				GL11.glDepthMask(false);
				wr.setVertexFormat(DefaultVertexFormats.field_176600_a);

				wr.setTranslation(-xOff, -yOff, -zOff);

				//Controls Draw Sphere or not
				if (true)
				{
					float density = 0.2253521F;

					int intervals = 24 + (int)(density * 72.0F);

					double[] xs = new double[intervals * (intervals / 2 + 1)];
					double[] zs = new double[intervals * (intervals / 2 + 1)];
					double[] ys = new double[intervals * (intervals / 2 + 1)];

					for (double phi = 0.0D; phi < 6.283185307179586D; phi += 6.283185307179586D / intervals)
					{
						for (double theta = 0.0D; theta < 3.141592653589793D; theta += 3.141592653589793D / (intervals / 2))
						{
							double dx = vRad * Math.sin(phi) * Math.cos(theta);
							double dz = vRad * Math.sin(phi) * Math.sin(theta);
							double dy = vRad * Math.cos(phi);

							int index = (int)(phi / (6.283185307179586D / intervals) + intervals * theta / (3.141592653589793D / (intervals / 2)));

							xs[index] = (vCen.getX() + dx);
							zs[index] = (vCen.getZ() + dz);
							ys[index] = (vCen.getY() + dy);
						}
					}

					wr.startDrawing(0);
					for (int t = 0; t < intervals * (intervals / 2 + 1); t++){
						wr.addVertex(xs[t], ys[t], zs[t]);
					}
					Tessellator.getInstance().draw();
				}

				Iterator villageDoors = Doors.iterator();

				for (Iterator i$ = Doors.iterator(); i$.hasNext(); ) { 
					Object dObj = i$.next();
				
					if ((dObj instanceof myVec3))
					{
						wr.startDrawing(1);
						myVec3 doorInfo = (myVec3)dObj;
						wr.addVertex(doorInfo.getX(), doorInfo.getY(), doorInfo.getZ());
						wr.addVertex(vCen.getX(), vCen.getY(), vCen.getZ());
						Tessellator.getInstance().draw();
					}
				}
				wr.startDrawing(0);
				wr.addVertex(vCen.getX(), vCen.getY(), vCen.getZ());
				Tessellator.getInstance().draw();
				if (true)
				{
					renderGolemBox(wr, vCen, c);
				}

				GL11.glDisable(3042);
				GL11.glEnable(3553);
				GL11.glDepthMask(true);
				wr.setTranslation(0.0D, 0.0D, 0.0D);
			}
		}
	}

	private static void renderGolemBox(WorldRenderer wr, myVec3 vCen, int c)
	{
		GL11.glPolygonMode(1032, 6914);
		GL11.glDisable(2884);

		wr.startDrawing(7);

		GL11.glLineWidth(2.0F);

		setWallColor((c - 1) % 6);
		GL11.glBlendFunc(772, 1);

		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() - 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() - 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() + 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() + 2.999D, vCen.getZ() - 7.999D);

		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() + 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() + 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() + 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() + 2.999D, vCen.getZ() + 7.999D);

		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() + 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() - 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() - 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() + 2.999D, vCen.getZ() + 7.999D);

		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() + 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() + 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() - 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() - 2.999D, vCen.getZ() + 7.999D);

		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() - 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() - 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() - 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() - 2.999D, vCen.getZ() - 7.999D);

		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() - 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() + 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() + 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() - 2.999D, vCen.getZ() - 7.999D);

		Tessellator.getInstance().draw();

		setColor((c - 1) % 6);
		GL11.glBlendFunc(1, 32769);

		wr.startDrawing(7);
		GL11.glPolygonMode(1032, 6913);

		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() - 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() - 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() + 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() + 2.999D, vCen.getZ() - 7.999D);

		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() + 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() + 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() + 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() + 2.999D, vCen.getZ() + 7.999D);

		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() + 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() - 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() - 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() + 2.999D, vCen.getZ() + 7.999D);

		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() + 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() + 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() - 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() - 2.999D, vCen.getZ() + 7.999D);

		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() - 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() - 2.999D, vCen.getZ() + 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() - 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() - 2.999D, vCen.getZ() - 7.999D);

		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() - 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() + 7.999D, vCen.getY() + 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() + 2.999D, vCen.getZ() - 7.999D);
		wr.addVertex(vCen.getX() - 7.999D, vCen.getY() - 2.999D, vCen.getZ() - 7.999D);

		Tessellator.getInstance().draw();

		GL11.glEnable(2852);
		GL11.glLineStipple(5, (short)-30584);

		wr.startDrawing(1);

		for (int xi = -8; xi <= 8; xi++)
		{
			for (int yi = -3; yi <= 3; yi++)
			{
				if ((xi == -8) || (xi == 8) || (yi == -3) || (yi == 3))
				{
					wr.addVertex(vCen.getX() + xi, vCen.getY() + yi, vCen.getZ() - 8);
					wr.addVertex(vCen.getX() + xi, vCen.getY() + yi, vCen.getZ() + 8);

					wr.addVertex(vCen.getX() - 8, vCen.getY() + yi, vCen.getZ() + xi);
					wr.addVertex(vCen.getX() + 8, vCen.getY() + yi, vCen.getZ() + xi);
				}
			}

			for (int yi = -8; yi <= 8; yi++)
			{
				if ((xi == -8) || (xi == 8) || (yi == -8) || (yi == 8))
				{
					wr.addVertex(vCen.getX() + xi, vCen.getY() + 3, vCen.getZ() + yi);
					wr.addVertex(vCen.getX() + xi, vCen.getY() - 3, vCen.getZ() + yi);
				}
			}
		}

		Tessellator.getInstance().draw();

		wr.setTranslation(0.0D, 0.0D, 0.0D);

		GL11.glDisable(2852);
		GL11.glPolygonMode(1032, 6914);
		GL11.glEnable(2884);
	}


	private static void setColor(int c)
	{
		if (c == 0)
		{
			GL11.glColor4f(1.0F, 0.0F, 0.0F, 1.0F);
		}
		else if (c == 4)
		{
			GL11.glColor4f(0.0F, 1.0F, 0.0F, 1.0F);
		}
		else if (c == 2)
		{
			GL11.glColor4f(0.0F, 0.0F, 1.0F, 1.0F);
		}
		else if (c == 5)
		{
			GL11.glColor4f(1.0F, 1.0F, 0.0F, 1.0F);
		}
		else if (c == 1)
		{
			GL11.glColor4f(1.0F, 0.0F, 1.0F, 1.0F);
		}
		else if (c == 3)
		{
			GL11.glColor4f(0.0F, 1.0F, 1.0F, 1.0F);
		}
		else
		{
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		}
	}
	private static void setWallColor(int c)
	{
		if (c == 0)
		{
			GL11.glColor4f(0.12F, 0.0F, 0.0F, 0);
		}
		else if (c == 4)
		{
			GL11.glColor4f(0.0F, 0.1F, 0.0F, 0);
		}
		else if (c == 2)
		{
			GL11.glColor4f(0.0F, 0.0F, 0.125F, 0);
		}
		else if (c == 5)
		{
			GL11.glColor4f(0.105F, 0.105F, 0.0F, 0);
		}
		else if (c == 1)
		{
			GL11.glColor4f(0.105F, 0.0F, 0.105F, 0);
		}
		else if (c == 3)
		{
			GL11.glColor4f(0.0F, 0.105F, 0.105F, 0);
		}
		else
		{
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 0);
		}
	}
}
