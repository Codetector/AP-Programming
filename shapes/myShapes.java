import javafx.scene.shape.RectangleBuilder;

/**
 * Write a description of class myShapes here.
 *
 * @author Chris Madeira
 * @version July 14, 2014
 */
public class myShapes {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        c1.makeVisible();
        c1.setpos(30, 30);
        c1.changeColor("blue");
        Circle c2 = new Circle();
        c2.makeVisible();
        c2.setpos(100, 30);
        c2.changeColor("blue");
        Triangle t = new Triangle();
        t.setpos(80, 90);
        t.makeVisible();
        Square sq = new Square();
        sq.makeVisible();
        sq.changeColor("red");
        sq.changeSize(80);
        sq.moveHorizontal(-20);
        sq.moveVertical(100);


    }

}
