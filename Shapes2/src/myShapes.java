import javafx.scene.shape.RectangleBuilder;

/**
 * Write a description of class myShapes here.
 *
 * @author Chris Madeira
 * @version July 14, 2014
 */
public class myShapes {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        c1.makeVisible();
        c1.changeColor("blue");
        Circle c2 = new Circle("red",100);
        c2.setxPosition(100);
        c2.makeVisible();
    }
}
