import java.awt.*;

/**
 * A triangle that can be manipulated and that draws itself on a canvas.
 *
 * @version 1.0  (15 July 2000)
 * @author Michael Kolling and David J. Barnes
 */

public class Triangle extends Shapes {
    private int height;
    private int width;

    /**
     * Create a new triangle at default position with default color.
     */
    public Triangle() {
        height = 30;
        width = 40;
    }

    public Triangle(String color, int height, int width) {
        super(color);
        this.height = height;
        this.width = width;
    }

    /**
     * Make this triangle visible. If it was already visible, do nothing.
     */
    @Override
    public void makeVisible() {
        super.makeVisible();
        draw();
    }

    @Override
    public void makeInvisible() {
        erase();
        super.makeInvisible();
    }

    public void moveHorizontal(int distance) {
        erase();
        super.moveHorizontal(distance);
        draw();
    }

    /**
     * Move the triangle vertically by 'distance' pixels.
     */
    public void moveVertical(int distance) {
        erase();
        super.moveVertical(distance);
        draw();
    }


    /**
     * Change the size to the new size (in pixels). Size must be >= 0.
     */
    public void changeSize(int newHeight, int newWidth) {
        erase();
        height = newHeight;
        width = newWidth;
        draw();
    }

    /**
     * Change the color. Valid colors are "red", "yellow", "blue", "green",
     * "magenta" and "black".
     */
    @Override
    public void changeColor(String newColor) {
        super.changeColor(newColor);
        draw();
    }

    /*
     * Draw the triangle with current specifications on screen.
     */
    private void draw() {
        if (super.isVisible()) {
            Canvas canvas = Canvas.getCanvas();
            int[] xpoints = {super.getxPosition(), super.getxPosition() + (width / 2), super.getxPosition() - (width / 2)};
            int[] ypoints = {super.getyPosition(), super.getyPosition() + height, super.getyPosition() + height};
            canvas.draw(this, super.getColor(), new Polygon(xpoints, ypoints, 3));
            canvas.wait(10);
        }
    }

    /*
     * Erase the triangle on screen.
     */
    private void erase() {
        if (super.isVisible()) {
            Canvas canvas = Canvas.getCanvas();
            canvas.erase(this);
        }
    }
}
