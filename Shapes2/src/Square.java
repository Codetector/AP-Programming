import java.awt.*;

/**
 * A square that can be manipulated and that draws itself on a canvas.
 *
 * @version 1.0  (15 July 2000)
 * @author Michael Kolling and David J. Barnes
 */

public class Square extends Shapes {
    public int size;


    /**
     * Create a new square at default position with default color.
     */
    public Square() {
        super();
        size = 30;
    }

    public Square(String color, int size) {
        super(color);
        this.size = size;
    }

    /**
     * Make this square visible. If it was already visible, do nothing.
     */
    @Override
    public void makeVisible() {
        super.makeVisible();
        draw();
    }

    /**
     * Make this square invisible. If it was already invisible, do nothing.
     */
    @Override
    public void makeInvisible() {
        erase();
        super.makeInvisible();
    }

    @Override
    public void moveHorizontal(int distance) {
        erase();
        super.moveHorizontal(distance);
        draw();
    }

    /**
     * Move the square vertically by 'distance' pixels.
     */
    @Override
    public void moveVertical(int distance) {
        erase();
        super.moveVertical(distance);
        draw();
    }


    /**
     * Change the size to the new size (in pixels). Size must be >= 0.
     */
    public void changeSize(int newSize) {
        erase();
        size = newSize;
        draw();
    }

    /**
     * Change the color. Valid colors are "red", "yellow", "blue", "green",
     * "magenta" and "black".
     */
    public void changeColor(String newColor) {
        super.changeColor(newColor);
        draw();
    }

    /*
     * Draw the square with current specifications on screen.
     */
    private void draw() {
        if (super.isVisible()) {
            Canvas canvas = Canvas.getCanvas();
            canvas.draw(this, super.getColor(),
                    new Rectangle(super.getxPosition(), super.getyPosition(), size, size));
            canvas.wait(10);
        }
    }

    /*
     * Erase the square on screen.
     */
    private void erase() {
        if (super.isVisible()) {
            Canvas canvas = Canvas.getCanvas();
            canvas.erase(this);
        }
    }
}
