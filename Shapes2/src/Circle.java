import java.awt.geom.Ellipse2D;

/**
 * A circle that can be manipulated and that draws itself on a canvas.
 *
 * @author Michael Kolling and David J. Barnes
 * @version 1.0  (15 July 2000)
 */

public class Circle extends Shapes {
    private int diameter;

    /**
     * Create a new circle at default position with default color.
     */
    public Circle() {
        super();
        diameter = 30;
    }

    /**
     *
     * @param color God dam color
     * @param radius God Dam Radius
     */
    public Circle(String color, int radius) {
        super(color);
        diameter = radius;
    }

    @Override
    /**
     * Make this circle visible. If it was already visible, do nothing.
     */
    public void makeVisible() {
        super.makeVisible();
        draw();
    }

    /**
     * Change the size to the new size (in pixels). Size must be >= 0.
     */
    public void changeSize(int newDiameter) {
        erase();
        diameter = newDiameter;
        draw();
    }

    /**
     * Make this circle invisible. If it was already invisible, do nothing.
     */
    public void makeInvisible() {
        super.makeInvisible();
        erase();
    }

    @Override
    public void moveHorizontal(int distance) {
        erase();
        super.moveHorizontal(distance);
        draw();
    }

    @Override
    public void moveVertical(int distance) {
        erase();
        super.moveVertical(distance);
        draw();
    }

    /*
         * Draw the circle with current specifications on screen.
         */
    private void draw() {
        if (super.isVisible()) {
            Canvas canvas = Canvas.getCanvas();
            canvas.draw(this, super.getColor(), new Ellipse2D.Double(super.getxPosition(), super.getyPosition(),
                    diameter, diameter));
            canvas.wait(10);
        }
    }

    /*
     * Erase the circle on screen.
     */
    private void erase() {
        if (super.isVisible()) {
            Canvas canvas = Canvas.getCanvas();
            canvas.erase(this);
        }
    }
}
